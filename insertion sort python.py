#!/usr/bin/env python
# coding: utf-8

# # Insertion Sort

# ## Bartosz Rydziński

# In[1]:


def Insertion_sort(tablica):
    for indeks in range(1,len(tablica)):
        i = indeks
        klucz = tablica[indeks]
        while i>0 and tablica[i-1] > klucz:
            tablica[i]= tablica[i-1]
            i -= 1
            tablica[i] = klucz


# In[2]:


do_posortowania = [int(x) for x in input('Wprowadź liczby oddzielając je spacjami:').split()]


# In[3]:


Insertion_sort(do_posortowania)


# **Wynik sortowania:**

# In[4]:


print(do_posortowania)


# **Badanie złożoności:**

# In[5]:


import numpy as np
import time
import matplotlib.pyplot as plt


# In[10]:


n=10
pomiary_czasu= []
liczby=[]
while n<1000:
    n_tablica = np.random.randint(2, size=n)
    start = time.time()
    Insertion_sort(n_tablica)
    czas = start- time.time()
    pomiary_czasu.append(-czas)
    liczby.append(n)
    n+=1
plt.plot(numbers, times, 'ro')
plt.xlabel("ilość danych")
plt.ylabel("czas wsekund")

